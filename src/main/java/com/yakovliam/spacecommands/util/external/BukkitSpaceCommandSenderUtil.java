package com.yakovliam.spacecommands.util.external;

import com.yakovliam.spaceapi.command.BukkitSpaceCommandSender;
import com.yakovliam.spaceapi.command.SpaceCommandSender;
import org.bukkit.entity.Player;

public class BukkitSpaceCommandSenderUtil {

    public static Player getPlayer(SpaceCommandSender commandSender) {
        if (!commandSender.isPlayer()) return null;

        return (Player) ((BukkitSpaceCommandSender) commandSender).getBukkitSender();
    }
}
