package com.yakovliam.spacecommands;

import com.yakovliam.spaceapi.abstraction.BukkitPlugin;
import com.yakovliam.spacecommands.stats.MetricsHandler;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;

public class SpaceCommands extends JavaPlugin {

    @Getter
    private static SpaceCommands instance;

    @Getter
    @Setter
    private BukkitPlugin api;


    @Override
    public void onLoad() {
        // set instance
        instance = this;
    }

    @Override
    public void onEnable() {
        // initialize API
        api = new BukkitPlugin(this);

        // ---

        // init metrics
        new MetricsHandler();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
