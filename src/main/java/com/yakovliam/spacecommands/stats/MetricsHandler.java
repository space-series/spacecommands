package com.yakovliam.spacecommands.stats;

import com.yakovliam.spacecommands.SpaceCommands;
import org.bstats.bukkit.Metrics;

public class MetricsHandler {

    public MetricsHandler() {
        init();
    }

    private static final int pluginId = 7604;

    public static void init() {
        // init metrics
        new Metrics(SpaceCommands.getInstance(), pluginId);
    }
}
